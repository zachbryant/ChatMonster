/*  
*   <ChatMonster, here to gobble up all of your unwanted chat.>
*   Copyright (C) 2013  Zach Bryant
*
*   This program is free software: you can redistribute it and/or modify
*   it under the terms of the GNU General Public License as published by
*   the Free Software Foundation, either version 3 of the License, or
*   (at your option) any later version.
*
*   This program is distributed in the hope that it will be useful,
*   but WITHOUT ANY WARRANTY; without even the implied warranty of
*   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*   GNU General Public License for more details.
*
*   You should have received a copy of the GNU General Public License
*   along with this program.  If not, see http://www.gnu.org/licenses/.
*/

package io.github.burntapples;

import java.io.File;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.logging.Level;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import org.bukkit.ChatColor;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.player.AsyncPlayerChatEvent;

public class ChatListener implements Listener
{
  private ChatMonster plugin;
  protected File logFile;
  protected FileConfiguration config;
  protected YamlConfiguration log;
  protected YamlConfiguration lang;
  protected boolean whitelist;
  protected boolean adreplace;
  protected boolean adWarn;
  protected boolean censor;
  protected boolean censorParseAll;
  protected boolean censorWarn;
  protected boolean eatspam;
  protected boolean spamWarn;
  protected boolean blockCaps;
  protected boolean enabled;
  protected boolean noBeginCaps;
  protected boolean deterBypass;
  protected boolean showCaughtAds;
  private boolean bes;
  protected ArrayList<String> whitelisted;
  protected ArrayList<String> findCensor;
  protected int frequency;
  protected int warnings;
  protected int adLimit;
  protected long expected;
  protected String toCensor;
  protected CMUtils utils;
  protected String original;
  private Player player;
  private String name;
  private String theIP;
  private boolean deterred=false;
  
  public ChatListener(ChatMonster pl)
  {
    plugin = pl;
    config = plugin.getConfig();
    logFile = new File(plugin.getDataFolder() + File.separator + "log.yml");
    log = YamlConfiguration.loadConfiguration(logFile);
    lang=pl.lang;
    getCMValues();
    utils = new CMUtils(this, pl);
  }
  
  protected CMUtils getUtils(){ return utils; }
  
  protected final void getCMValues()
  {
    whitelist = config.getBoolean("advertising.enabled.chat");
    adreplace = config.getBoolean("advertising.replace");
    censor = config.getBoolean("censor.enabled.chat");
    censorWarn = config.getBoolean("censor.warn");
    censorParseAll = config.getBoolean("censor.parse-all");
    eatspam = config.getBoolean("eatspam.enabled");
    spamWarn = config.getBoolean("eatspam.warn");
    whitelisted = ((ArrayList)config.getList("advertising.whitelisted"));
    frequency = config.getInt("eatspam.frequency");
    warnings = config.getInt("eatspam.warnings");
    adLimit = config.getInt("advertising.limit");
    adWarn = config.getBoolean("advertising.warn");
    blockCaps = config.getBoolean("eatspam.block-caps");
    toCensor = config.getString("censor.replace");
    findCensor = ((ArrayList)config.getStringList("censor.block"));
    enabled = config.getBoolean("chatmonster-enabled");
    bes = config.getBoolean("eatspam.block-excessive-symbols");
    noBeginCaps = config.getBoolean("eatspam.No-Begin-Caps");
    deterBypass = config.getBoolean("deter-bypass.enabled");
    showCaughtAds = config.getBoolean("deter-bypass.show-caught-ads");
  }
  
  
  @EventHandler(priority=EventPriority.HIGHEST)
  public void onPlayerChat(AsyncPlayerChatEvent chat){
    if(!enabled)
      return;
    player = chat.getPlayer();
    name = player.getName();
    if(!log.contains(name + ".warnings"))
      log.set(name + ".warnings", 0);
    if(!log.contains(name + ".second-offense"))
      log.set(name + ".second-offense", false);
    if(!log.contains(name + ".parseAll"))
      log.set(name + ".parseAll", false);
    if(!log.contains(name + ".last"))
      log.set(name + ".last", "Setting up ChatMonster but I doubt you will ever see this");
    if(!log.contains(name + ".time"))
      log.set(name + ".time", System.currentTimeMillis()-config.getLong("eatspam.duration"));
    if(!log.contains(name+".failed-last"))
      log.set(name+".failed-last", false);
    utils.saveLog();
    utils.reloadLog();
    original = chat.getMessage();
    
    if(!chat.isCancelled())
    {
        if(eatspam && !chat.getPlayer().hasPermission("chatmonster.bypass.spam"))
            chat = findSpam(chat, log.getLong(name + ".time"));
        if(!chat.isCancelled() &&whitelist && !chat.getPlayer().hasPermission("chatmonster.bypass.ad"))
            chat = findAd(chat);
        if(!chat.isCancelled() && censor && !chat.getPlayer().hasPermission("chatmonster.bypass.censor"))
            chat = findCensor(chat);
        if(!chat.isCancelled()){
            log.set(name + ".last", chat.getMessage());
            log.set(name + ".time", System.currentTimeMillis());
        }
      deterred = false;
      utils.saveLog();
      utils.reloadLog();
    }
  }
  
  private final AsyncPlayerChatEvent findAd(AsyncPlayerChatEvent c)
  {
      String[] msg = c.getMessage().replaceAll("(dot|DOT|Dot|dOt|doT|DOt|dOT|DoT|d0t|D0T|D0t|d0t|d0T|D0t|d0T|D0T)", ".").trim().split(" ");
      Pattern validHostname = Pattern.compile("^(?=(?:.*?[\\.\\,]){1})(?:[a-z][a-z0-9-]*[a-z0-9](?=[\\.,][a-z]|$)[\\.,:;|\\\\]?)+$");
      Pattern validIpAddress = Pattern.compile("^(?:(?:[0-9]|[1-9][0-9]|1[0-9]{2}|2[0-4][0-9]|25[0-5])\\.){3}(?:[0-9]|[1-9][0-9]|1[0-9]{2}|2[0-4][0-9]|25[0-5])(?::\\d*)?$", 2);
    
      boolean found = false;
      int rand = (int)(Math.random() * whitelisted.size());
      String replace = (String)whitelisted.get(rand);
      StringBuilder end =new StringBuilder();
    
      for(int x=0;x<msg.length;x++){
          for(int y = 0; y < whitelisted.size(); y++)
              if(Pattern.compile(Pattern.quote((String)whitelisted.get(y)), 2).matcher(msg[x]).find())
                  return c;
          String tempIP=msg[x].trim().toLowerCase().replaceAll("[\\(\\)!@#\\$%\\^\\s\\&\\*;\"'\\?><~`,\\\\a-zA-Z]","");
          String tempHost=msg[x].trim().toLowerCase().replaceAll("[\\d\\s\\(\\)!@#\\$%\\^\\s\\&\\*:;\"'\\?><~`,\\\\]","");
          Matcher matchIP = validIpAddress.matcher(tempIP);
          while(matchIP.find()){
              theIP = msg[x];
              if(adreplace)
                  msg[x]=replace;
              else
                  c.setCancelled(true);
          
              found = true;
          }
          Matcher matchHost = validHostname.matcher(tempHost);
          while(matchHost.find()){
              theIP = msg[x];
              if(adreplace)
                  msg[x]=replace;
              else
                  c.setCancelled(true);
              found = true;
          }
          tempHost =msg[x].toLowerCase();
          String[] d = "www. http .com .net .org .ru .uk .us .fr .co .ca".split(" ");
          for(String s: d)
              if(tempHost.contains(s))
                  if(adreplace)
                      msg[x]=replace;
                  else
                      msg[x]="";
          end.append(msg[x]+" ");
      }
      
      if(found){
          c.setMessage(end.toString().trim());
          if(c.getMessage().length()==0)
              c.setCancelled(true);
          if(deterBypass){
              deterBypass(c,"ad");
          }
          if(!adreplace)
              c.setCancelled(true);
          if(adWarn&&!deterBypass)
              utils.warn(name, player, 1, lang.getString("adv")+".", "advertising");
      }
      return c;
  }
  //@TODO 3=e 1=i/l o=0 
  private final AsyncPlayerChatEvent findCensor(AsyncPlayerChatEvent c)
  {
    boolean found=false;
    ArrayList<String> msgList = new ArrayList<String>(Arrays.asList(c.getMessage().split(" ")));
    
    for(int x = 0; x < msgList.size(); x++)
    {
      for(int y = 0; y < findCensor.size(); y++)
      {
        String findx = msgList.get(x).toLowerCase().replaceAll("[\\W]", "");
        String findy = findCensor.get(y).toLowerCase();
        if(findx.contains(findy))
        {
            found=true;
          if(toCensor.equalsIgnoreCase("false"))
          {
            msgList.remove(x);
            x--;
            if(msgList.isEmpty())
            {
                c.setCancelled(true);
                return c;
            }
            else {
              String newMsg = "";
              for(String s : msgList)
                newMsg+=s + " ";
              
              c.setMessage(newMsg);
              msgList = new ArrayList(Arrays.asList(newMsg.split(" ")));
            }
          }
          else {
            msgList.set(x, findx.replaceAll(findy,toCensor));
            String message = "";
            for(String s : msgList)
              message+= s + " ";
            c.setMessage(message.trim());
            msgList = new ArrayList(Arrays.asList(c.getMessage().split(" ")));
          }  
        }
      }
    }
    if(found){
        if(deterBypass){
            deterBypass(c,"censor");
        }
        if(censorWarn&&!deterBypass)
            utils.warn(name, player, 1, lang.getString("speakWrong"), "censor");
    }
    return c;
  }
  
  private final AsyncPlayerChatEvent findSpam(AsyncPlayerChatEvent c, long time)
  {
    String msg = c.getMessage();
    long duration = config.getLong("eatspam.duration");
    expected = (time + duration);
    
    if(System.currentTimeMillis() < expected){
      player.sendMessage(ChatColor.RED + lang.getString("needWait") + (((double)(Math.round(((expected - System.currentTimeMillis()) / 1000.0)*100)))/100) + lang.getString("secBefore"));
      log.set(name+".failed-last",true);
      c.setCancelled(true);
      return c;
    }
    if(msg.length()>3&&utils.findIfCaps(msg)){
      c.setMessage(msg.toLowerCase());
      msg = c.getMessage();
    }
    if(noBeginCaps)
    {
      c.setMessage(utils.beginCaps(msg));
      msg = c.getMessage();
    }
    if(bes){
      c.setMessage(msg.replaceAll("[\\W]{4}", ""));
      msg = c.getMessage();
    }
    if(log.getBoolean(name + ".parseAll")){
      c.setMessage(msg.replaceAll("[\\W]{2,}", ""));
      msg = c.getMessage();
    }
    
    ArrayList<String> refMsg = new ArrayList<String>(Arrays.asList(msg.replaceAll("\\W","").split("")));
    ArrayList<String> refLast = new ArrayList<String>(Arrays.asList(log.getString(name + ".last").replaceAll("\\W","").split("")));
    int letsInCmn = 0;
    double big = Math.max(refMsg.size(), refLast.size());
    
    for(int x = 0; x < refMsg.size(); x++){
      for(int y = 0; y < refLast.size(); y++)
      {
        if(refMsg.get(x).compareTo(refLast.get(y))==0)
          letsInCmn++; 
        if(x<refMsg.size()-1)
            x++;
      }
    }
    double percentage = Double.valueOf(letsInCmn / big);
    double similarity = config.getDouble("eatspam.similarity");
    
    if((similarity < 0.0) || (similarity > 1.0))
        similarity = 0.65;
    if(percentage > similarity)
    {
        c.setCancelled(true);
        if(spamWarn&&!deterBypass)
            utils.warn(name, player, 1, lang.getString("spamming"), "eatspam");
        if(deterBypass)
            deterBypass(c,"spam");
    }
    if(c.getMessage().length()==0)
        c.setCancelled(true);
    
    return c;
  }
  protected AsyncPlayerChatEvent deterBypass(AsyncPlayerChatEvent c, String who){
      if(deterred)
          return c;
      if(who.equalsIgnoreCase("ad")){
          if(showCaughtAds){
              for(Player p: plugin.getServer().getOnlinePlayers())
                  if(p.hasPermission("chatmonser.vca"))
                      p.sendMessage(""+ChatColor.RED+ChatColor.BOLD+"User " +ChatColor.RESET + name + ChatColor.RED+ChatColor.BOLD+" attempted to show the following IP: "+ChatColor.RESET+theIP);
              plugin.getLogger().log(Level.INFO, ""+ChatColor.RED+ChatColor.BOLD+"User " +ChatColor.RESET + name + ChatColor.RED+ChatColor.BOLD+" attempted to show the folliwing IP: "+ChatColor.RESET+theIP);
      }
      else
          c.setCancelled(true);
      }
      if(who.equalsIgnoreCase("spam"))
          c.setCancelled(true);
      deterred=true;
      c.getRecipients().remove(player);
      player.sendMessage(String.format(c.getFormat(),player.getDisplayName(),original));
      return c;
  }
}
