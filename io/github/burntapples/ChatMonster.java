/*  
*   <ChatMonster, here to gobble up all of your unwanted chat.>
*   Copyright (C) 2013  Zach Bryant
*
*   This program is free software: you can redistribute it and/or modify
*   it under the terms of the GNU General Public License as published by
*   the Free Software Foundation, either version 3 of the License, or
*   (at your option) any later version.
*
*   This program is distributed in the hope that it will be useful,
*   but WITHOUT ANY WARRANTY; without even the implied warranty of
*   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*   GNU General Public License for more details.
*
*   You should have received a copy of the GNU General Public License
*   along with this program.  If not, see http://www.gnu.org/licenses/.
*/
package io.github.burntapples;

import java.io.File;
import org.apache.commons.lang.StringUtils;
import org.bukkit.ChatColor;
import org.bukkit.command.CommandSender;
import org.bukkit.command.ConsoleCommandSender;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.entity.Player;
import org.bukkit.event.HandlerList;
import org.bukkit.plugin.PluginManager;
import org.bukkit.plugin.java.JavaPlugin;

public class ChatMonster extends JavaPlugin{
    
    protected FileConfiguration config;
    protected CMUtils utils;
    protected ChatListener listener;
    protected SignListener sign;
    protected JoinListener joinListener;
    protected BookListener book;
    protected Updater updater;
    protected YamlConfiguration lang;
    private File langFile;
    private PluginManager pm;
    public static boolean update=false;
    public static String name="";
    
    @Override
    public void onEnable()
    {      
        if(!new File(getDataFolder(), "config.yml").exists())
        {
            saveDefaultConfig();
	}
        
	reloadConfig();
        File cmlog = new File(getDataFolder()+File.separator+"log.yml");
            if(!cmlog.exists())
            {
                getDataFolder().mkdir();
                try{
                    cmlog.createNewFile();
                }
                catch(Exception ioe){ioe.printStackTrace();}
            }
            
        
        config = getConfig();
        String L = config.getString("lang").toUpperCase();
        langFile = new File(getDataFolder() + File.separator + "lang_"+L+".yml");
        if(!langFile.exists()){
            saveResource("lang_"+L+".yml", true);
        }
        try{
            lang = YamlConfiguration.loadConfiguration(langFile);
        }
        catch(Exception e){
            lang = YamlConfiguration.loadConfiguration(langFile);
            getLogger().severe("Unable to load your language file! Using ENGLISH instead.");
            if(!config.getBoolean("suppress-output"))
                e.printStackTrace();
        }
        pm = getServer().getPluginManager();
        listener=new ChatListener(this);
        sign=new SignListener(this, listener);
        joinListener = new JoinListener(this);
        book = new BookListener(this,listener);
        utils=listener.getUtils();
        utils.correctConfig();
        
        pm.registerEvents(listener, this);
        pm.registerEvents(sign, this);
        pm.registerEvents(book, this);
        pm.registerEvents(joinListener, this);
        
        if(!config.getBoolean("chatmonster-enabled"))
            getLogger().info(lang.getString("chatmonster-enabled"));
        
        getCommand("cm").setExecutor(utils);
        getCommand("cm clearwarnings").setExecutor(utils);
        getCommand("cm check").setExecutor(utils);
        getCommand("cm warn").setExecutor(utils);
        getCommand("cm reload").setExecutor(utils);
        getCommand("cm configure").setExecutor(utils);
        getCommand("cm parse").setExecutor(utils);
        getCommand("cm toggle").setExecutor(utils);   
        getCommand("cm help").setExecutor(utils);
        getCommand("cm update").setExecutor(utils);

        if(config.getBoolean("auto-update.notify")){
            if(updateCheck()){
                Player[] list = getServer().getOnlinePlayers();
                for(Player p: list)
                    if(p.hasPermission("chatmonster.update"))
                        p.sendMessage(ChatColor.GREEN+lang.getString("update-ready.1")+ChatColor.WHITE+lang.getString("update-ready.2")+ChatColor.GREEN+lang.getString("update-ready.3")+ChatColor.WHITE+lang.getString("update-ready.4")+ ChatColor.GREEN+lang.getString("update-ready.5"));
            }
        }
        
        if(config.getBoolean("auto-update.download")&&updateCheck()){
            updater = new Updater(this, 62373, this.getFile(), Updater.UpdateType.DEFAULT, true);
            update=false;
        }
    }
    
    @Override
    public void onDisable()
    {
        saveConfig();
        utils.saveLog();
        utils.end();
        HandlerList.unregisterAll(this);
    }
    
    void update(){updater = new Updater(this, 62373, this.getFile(), Updater.UpdateType.NO_VERSION_CHECK, true);}
    boolean updateCheck(){
        updater = new Updater(this, 62373, this.getFile(), Updater.UpdateType.NO_DOWNLOAD, false);
        update = updater.getResult()== Updater.UpdateResult.UPDATE_AVAILABLE;
        return update;
    }
    
    public String getUpdateName(){return updater.getLatestName();}
    public boolean getUpdateStatus(){return updater.getResult()== Updater.UpdateResult.UPDATE_AVAILABLE;}
    public String getLatestName(){return updater.getLatestName();}
    public String getLatestVersion(){return updater.getLatestGameVersion();}
    protected YamlConfiguration getLang(){return lang;}
    protected void displayHelp(CommandSender sender, int page)
    {
        final String HELP = lang.getString("help.helpTrans");
        final String PAGE = lang.getString("help.pageTrans");
        final String PLAYER = lang.getString("help.playerTrans");
        final String CHECK = lang.getString("help.checkTrans");
        final String ADD = lang.getString("help.addTrans");
        final String NUMBER = lang.getString("help.numberTrans");
        final String REASON = lang.getString("help.reasonTrans");
        final String PUNISHMENT = lang.getString("help.punishmentTrans");
        final String BYPASS = lang.getString("help.bypassTrans");
        final String RELOAD = lang.getString("help.reloadTrans");
        final String TOGGLE = lang.getString("help.toggleTrans");
        final String PATH = lang.getString("help.pathTrans");
        final String VALUE = lang.getString("help.valueTrans");
        final String CONFIGURE = lang.getString("help.configureTrans");
        final String UPDATE = lang.getString("help.updateTrans");
        final String CW = lang.getString("help.cwTrans");
                
        sender.sendMessage("-=-=-=-=-=-=-=-___"+ChatColor.GREEN+"ChatMonster "+StringUtils.capitalize(HELP)+ChatColor.WHITE+"___-=-=-=-=-=-=-=-");
        if(sender instanceof ConsoleCommandSender)
            sender.sendMessage(ChatColor.WHITE+"               ["+ChatColor.GREEN+lang.getString("help.opt")+ChatColor.WHITE+"]  <"+ChatColor.GREEN+lang.getString("help.req")+ChatColor.WHITE+">");
        else
            sender.sendMessage(ChatColor.WHITE+"                        ["+ChatColor.GREEN+lang.getString("help.opt")+ChatColor.WHITE+"]  <"+ChatColor.GREEN+lang.getString("help.req")+ChatColor.WHITE+">");
        if(page<=0 || page >3){
            sender.sendMessage(ChatColor.GREEN+lang.getString("help.bounds"));
        }
        if(page==1){
            sender.sendMessage(ChatColor.GREEN+"/cm ["+HELP+"] ["+PAGE+"]"+ChatColor.WHITE+lang.getString("help.help"));
            sender.sendMessage(ChatColor.GREEN+"/cm "+CW+" <"+PLAYER+"> "+ChatColor.WHITE+lang.getString("help.cw"));
            sender.sendMessage(ChatColor.GREEN+"/cm "+CHECK+" <"+PLAYER+"> "+ChatColor.WHITE+lang.getString("help.check"));
            sender.sendMessage(ChatColor.GREEN+"/cm "+ADD+" <"+PLAYER+"> <"+NUMBER+"> <"+REASON+"> ["+PUNISHMENT+"]"+ChatColor.WHITE+lang.getString("help.add"));
        }
        if(page==2){
            sender.sendMessage(ChatColor.GREEN+"/cm "+BYPASS+" <"+PLAYER+">"+ChatColor.WHITE+lang.getString("help.bypass"));
            sender.sendMessage(ChatColor.GREEN+"/cm "+RELOAD+ChatColor.WHITE+lang.getString("help.reload"));
            sender.sendMessage(ChatColor.GREEN+"/cm "+TOGGLE+ChatColor.WHITE+lang.getString("help.toggle"));
            sender.sendMessage(ChatColor.GREEN+"/cm "+CONFIGURE+" <"+PATH+"> <"+VALUE+">"+ChatColor.WHITE+lang.getString("help.conf"));
        }
        if(page==3){
            sender.sendMessage(ChatColor.GREEN+"/cm "+UPDATE+ChatColor.WHITE+lang.getString("help.update"));
        }
        sender.sendMessage(ChatColor.GREEN+lang.getString("help.page")+ChatColor.WHITE+page+ChatColor.GREEN+lang.getString("help.of")+ChatColor.WHITE+"3");
    }
    
    protected void sendWrongSyntax(CommandSender sender)
    {
        sender.sendMessage(ChatColor.RED+lang.getString("unknown.1")+ChatColor.GREEN+"/cm "+lang.getString("help.helpTrans")+ChatColor.RED+lang.getString("unknown.2"));
    }
    
    protected void sendNoPerms(CommandSender sender)
    {
        sender.sendMessage(ChatColor.RED+lang.getString("denied"));
    }
    
    public static void main(String[] args) {
    }
}
