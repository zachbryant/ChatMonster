/*  
*   <ChatMonster, here to gobble up all of your unwanted chat.>
*   Copyright (C) 2013  Zach Bryant
*
*   This program is free software: you can redistribute it and/or modify
*   it under the terms of the GNU General Public License as published by
*   the Free Software Foundation, either version 3 of the License, or
*   (at your option) any later version.
*
*   This program is distributed in the hope that it will be useful,
*   but WITHOUT ANY WARRANTY; without even the implied warranty of
*   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*   GNU General Public License for more details.
*
*   You should have received a copy of the GNU General Public License
*   along with this program.  If not, see http://www.gnu.org/licenses/.
*/
package io.github.burntapples;

import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerEditBookEvent;
import org.bukkit.inventory.meta.BookMeta;

public class BookListener implements Listener{
    private ChatMonster pl;
    private ChatListener cl;
    private boolean censor,advertising;
    
    public BookListener(ChatMonster plugin,ChatListener chat){
        pl = plugin;
        cl = chat;
        updateValues();
    }
    final void updateValues(){
        pl.config=pl.getConfig();
        censor = pl.config.getBoolean("censor.enabled.books");
        advertising = pl.config.getBoolean("advertising.enabled.books");
    }
    @EventHandler(priority=EventPriority.HIGHEST)
    private void onBookEdit(PlayerEditBookEvent e){
        if (!cl.enabled)
            return;
          String name = e.getPlayer().getName();
        if (!cl.log.contains(name + ".warnings"))
            cl.log.set(name + ".warnings", 0);
        if (!cl.log.contains(name + ".second-offense"))
            cl.log.set(name + ".second-offense", false);
        cl.utils.saveLog();
        cl.utils.reloadLog();
        if(!e.isCancelled()){
            if(censor&&!e.getPlayer().hasPermission("chatmonster.bypass.censor"))
                e=censor(e);
            if(advertising&&!e.getPlayer().hasPermission("chatmonster.bypass.ad"))
                e=advertising(e);
        }
        
    }
    private PlayerEditBookEvent censor(PlayerEditBookEvent e){
        Player p = e.getPlayer();
        BookMeta info = e.getNewBookMeta();
        List<String> pages = new ArrayList<String>(info.getPages());
        
        for(int x=0;x<pages.size();x++){
            String[] thing= pages.get(x).split(" ");
            for(int i=0;i<thing.length;i++){
                String temp = thing[i].replaceAll("[\\W\\d]","").toLowerCase();
                for(int y=0;y<cl.findCensor.size();y++){
                    String findy = cl.findCensor.get(y).toLowerCase();
                    if(temp.contains(findy)){
                        if(cl.toCensor.equalsIgnoreCase("false"))
                            temp=temp.replaceAll(findy, "");
                        else
                            temp=temp.replaceAll(findy,cl.toCensor);
                        thing[i]=temp;
                    }
                }
            }
            StringBuilder sb = new StringBuilder();
            for(String s:thing)
                sb.append(s+" ");
            pages.set(x,sb.toString().trim());
        }
        info.setPages(pages);
        e.setNewBookMeta(info);
        return e;
    }
    private PlayerEditBookEvent advertising(PlayerEditBookEvent e){
        Player p = e.getPlayer();
        BookMeta info = e.getNewBookMeta();
        List<String> pages = new ArrayList<String>(info.getPages());
        Pattern validHostname = Pattern.compile("^(?=(?:.*?[\\.\\,]){1})(?:[a-z][a-z0-9-]*[a-z0-9](?=[\\.,][a-z]|$)[\\.,:;|\\\\]?)+$");
        Pattern validIpAddress = Pattern.compile("^(?:(?:[0-9]|[1-9][0-9]|1[0-9]{2}|2[0-4][0-9]|25[0-5])\\.){3}(?:[0-9]|[1-9][0-9]|1[0-9]{2}|2[0-4][0-9]|25[0-5])(?::\\d*)?$", 2);
        boolean found = false;
        int rand = (int)(Math.random() * cl.whitelisted.size());
        String replace = (String)cl.whitelisted.get(rand);
        StringBuilder end = new StringBuilder();
        
        for(int i=0;i<pages.size();i++){
            String[] msg = pages.get(i).trim().replaceAll("(dot|DOT|Dot|dOt|doT|DOt|dOT|DoT|d0t|D0T|D0t|d0t|d0T|D0t|d0T|D0T)", ".").split(" ");
            for(int x=0;x<msg.length;x++){
                for(int y = 0; y < cl.whitelisted.size(); y++)
                    if(Pattern.compile(Pattern.quote((String)cl.whitelisted.get(y)), 2).matcher(msg[x]).find())
                        return e;
                String tempIP=msg[x].trim().toLowerCase().replaceAll("[\\(\\)!@#\\$%\\^\\s\\&\\*;\"'\\?><~`,\\\\a-zA-Z]","");
                String tempHost=msg[x].trim().toLowerCase().replaceAll("[\\d\\s\\(\\)!@#\\$%\\^\\s\\&\\*:;\"'\\?><~`,\\\\]","");
                Matcher matchIP = validIpAddress.matcher(tempIP);
                while(matchIP.find()){
                    if(cl.adreplace)
                        msg[x]=replace;
                    else
                        e.setCancelled(true);
                    found = true;
                }
                Matcher matchHost = validHostname.matcher(tempHost);
                while(matchHost.find()){
                    if(cl.adreplace)
                        msg[x]=replace;
                    else
                        e.setCancelled(true);
                    found = true;
                }
                tempHost =msg[x].toLowerCase();
                String[] d = "www. http .com .net .org .ru .uk .us .fr .co .ca".split(" ");
                for(String s: d)
                    if(tempHost.contains(s))
                        if(cl.adreplace)
                            msg[x]=replace;
                        else
                            msg[x]="";
                end.append(msg[x]+" ");
            }
            pages.set(i,end.toString());
        }
        if(found)
            info.setPages(pages);
      e.setNewBookMeta(info);
      return e;
    }
}
